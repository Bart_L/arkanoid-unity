﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelMenu : MonoBehaviour {
    [SerializeField]
    string LevelName;
	// Use this for initialization
	void Start ()
    {
        GetComponentInChildren<Text>().text = LevelName;
        GetComponent<Button>().onClick.AddListener(ChangeScene);
	}

    void ChangeScene()
    {
        SceneManager.LoadScene("game");
        PlayerPrefs.SetString("current_level", LevelName);

    }
}
