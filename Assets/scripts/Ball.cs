﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Ball : MonoBehaviour {
    public float InitialSpeed = 5f;

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody2D>().velocity = Random.insideUnitCircle.normalized * InitialSpeed;
	}
}
