﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Paddle : MonoBehaviour {

    Rigidbody2D Rigidbody;

    public float Speed = 5f;
    public float MaxPosition = 5.5f;

	// Use this for initialization
	void Start () {
        Rigidbody = GetComponent<Rigidbody2D>();
	}
	
	void FixedUpdate () {
        var targetDirection = Vector3.zero;

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            targetDirection = Vector3.left;
        } 
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            targetDirection = Vector3.right;
        }

        Rigidbody.velocity = targetDirection * Speed;

        //  limit positionX of the paddle
        var positionX = transform.position.x;
        positionX = Mathf.Clamp(positionX, -MaxPosition, MaxPosition);
        transform.position = new Vector3(positionX, transform.position.y);

        /* alternative way of limiting positionX of the paddle
        if (transform.position.x < -5.5f)
        {
            transform.position = new Vector3(-MaxPosition, transform.position.y);
        }
        if (transform.position.x > 5.5f)
        {
            transform.position = new Vector3(MaxPosition, transform.position.y);
        }
        */
    }
}
