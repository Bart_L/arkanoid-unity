﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EscapeHandler : MonoBehaviour
{

    public string LevelName = "menu";

    // Update is called once per frame
    void Update()
    {
        if (!Input.GetKey(KeyCode.Escape)) return;

        if (string.IsNullOrEmpty(LevelName))
        {
            Application.Quit();
        }
        else
        {
            SceneManager.LoadScene(LevelName);
        }
    }
}
