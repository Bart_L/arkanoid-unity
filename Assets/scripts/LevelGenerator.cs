﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using UnityEditor;
using UnityEngine;

[System.Serializable]

public class BlockType
{
    public char ColorCharacter;
    public Sprite Sprite;

}

public class LevelGenerator : MonoBehaviour {

    [SerializeField]
    GameObject BlockPrefab;

    [SerializeField]
    BlockType[] BlockTypes;

	// Use this for initialization
	public void GenerateLevel (string LevelName) {
        var level = LoadLevelFromTextFile(LevelName);
        GenerateBlocks(level);
    }


    BlockType[][] LoadLevelFromTextFile(string name)
    {
        var path = "Assets/levels/" + name + ".txt";
        var text = AssetDatabase.LoadAssetAtPath<TextAsset>(path).text;
        // split lines of the file where line ends with NewLine
        var lines = Regex.Split(text, System.Environment.NewLine);

        return ParseLines(lines);
    }

    BlockType ParseCharacter(char character)
    {
        return BlockTypes
            .FirstOrDefault(block => block.ColorCharacter == character);
    }

    BlockType[] ParseLine(string text)
    {
        return text
            .Select(ParseCharacter)
            .ToArray();
    }

    BlockType[][] ParseLines(string[] text)
    {
        return text
            .Select(ParseLine)
            .ToArray();
    }


    void GenerateBlocks(BlockType[][] blocks)
    {
        var height = blocks.Length;
        var width = blocks[0].Length;

        for (int x=0; x<width; x++)
        {
            for (int y = 0; y <height; y++)
            {
                var block = blocks[y][x];
                if (block == null)
                    continue;

                // move position of whole level to the center of the scene (case of y: because of the direction of gnereting blocks)
                var position = new Vector2(x - width/2f, -(y - height/2f));

                GenerateBlock(block.Sprite, position);
            }
        }
    }

    void GenerateBlock(Sprite sprite, Vector2 position)
    {
        var block = Instantiate(BlockPrefab);

        block.transform.parent = transform;
        block.transform.localPosition = position;

        block.GetComponent<SpriteRenderer>().sprite = sprite;
    }
}
