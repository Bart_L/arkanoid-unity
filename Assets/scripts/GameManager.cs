﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var levelName = PlayerPrefs.GetString("current_level");

        FindObjectOfType<LevelGenerator>().GenerateLevel(levelName);

        StartCoroutine(CheckIfLevelEndCoroutine());
    }

    IEnumerator CheckIfLevelEndCoroutine()
    {
        while(true)
        {
            CheckIfLevelEnd();
            yield return new WaitForSeconds(1f);
        }
    }

    void CheckIfLevelEnd()
    {
        var numberOfBlocks = FindObjectsOfType<Block>().Length;

        if (numberOfBlocks == 0)
        {
            SceneManager.LoadScene("menu");
        }
    }
}
